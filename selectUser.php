<!DOCTYPE html>
<?php
include "utilities.php";

$conn = createConnection();

$selectSql = "SELECT idUsuario,Name, LastName, email FROM users";
$result = mysqli_query($conn, $selectSql);

if (isset($_POST["newUserButton"]))
{
   header("Location: newuser.php");
  exit();
}

?>

<html lang="es-BO">
  <head>
    <title>El ahorcado - Seleccionar usuario</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/users.css">
  </head>
  <body>
  	<h1>Seleccionar usuario</h1>
        <h2>Nombre de Usuario</h2>
        <?php
        while( $row = mysqli_fetch_assoc($result) )
        { ?>
          <li><?= $row["Name"] ?> - <a href="Login.php?id=<?= $row["idUsuario"] ?>"> <?= $row["email"]?></a></li><br/><br/><br/>
        <?php
        }
    ?>
    <form method="POST">
      <input type="submit" name="newUserButton" value="Nuevo Usuario" />
    </form>
  </body>
</html>
