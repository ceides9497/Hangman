<!DOCTYPE html>
<?php
include "utilities.php";
function redirectToLogin()
{
  header("Location: selectUser.php");
  exit();
}

if (!isset($_GET["id"]))
  redirectToLogin();



function getUserPass($usId, $conn)
{
  $sql = "SELECT password FROM users WHERE idUsuario=?";

    $stmt = mysqli_stmt_init($conn);

    if (mysqli_stmt_prepare($stmt, $sql))
    {
      mysqli_stmt_bind_param($stmt, "i", $usId);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_bind_result($stmt, $password);
      mysqli_stmt_fetch($stmt);
      mysqli_stmt_close($stmt);

      return $password;
    }
    else
      return null;
}

$conn = createConnection();
$userId = $_GET["id"];

$userName = getUserName($userId, $conn);
$userPass = getUserPass($userId, $conn);

if (isset($_POST["loginButton"]))
{
  $ingresado=$_POST["PaswordTextBox"];

    if(password_verify($ingresado, $userPass))
      {

        header("Location: selectCategory.php?idUsuario=".$userId);
        exit();
      }
      else
      {
        echo '<script language="javascript">alert("Password erroneo");</script>';
        //redirectToLogin();
      }


}
?>
<html>
  <head>
  	 <meta charset="UTF-8">
     <link rel="stylesheet" type="text/css" href="css/users.css">
  </head>
  <body>
    <h1>Bienvenido</h1>
    <h2 style="font-size: 3em; margin:20px;"><u><?= $userName ?></u> </h2>

    <p style="float: left;">Contraseña:</p>
    <form method="POST" >
    <input type ="password"
           id="PaswordTextBox"
           name="PaswordTextBox" placeholder="Ingresa tu contraseña"/>
           </br>
      <input type="submit" name="loginButton" value="Login"  />
    </form>

  </body>
</html>
