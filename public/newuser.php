<!DOCTYPE html>
<?php
include_once "utilities.php";


function validarDatos($conf,$password,$name,$lastName)
{
  $resp = false;

  if($conf === $password && strlen($conf)>0 )
    {$resp = true;
      if(strlen($name)>=2)
      {
        $resp = true;
        if(strlen($lastName)>=2)
        {
          $resp = true;
        }
        else {
          $resp = false;
          echo '<script language="javascript">alert("El espacio de apellido debe tener 2 o mas caracteres");</script>';
        }
      }
      else {
        $resp = false;
        echo '<script language="javascript">alert("El espacio de nombre debe tener 2 o mas caracteres");</script>';
      }
    }
  else {
    $resp = false;
    echo '<script language="javascript">alert("Passwords no coinciden");</script>';
  }
  return $resp;
}

$conn = createConnection();

if (isset($_POST["createNewUserButton"]))
{


    $email=$_POST["EmailTextBox"];
    $password=$_POST["PaswordTextBox"];
    $confpass=$_POST["ConfPaswordTextBox"];
    $name=$_POST["NameTextBox"];
    $lastName=$_POST["lastNameTextBox"];


    $valido=validarDatos($confpass, $password,$name,$lastName);

    if($valido)
    {
      $sql = "INSERT INTO users (email, password, Name, LastName) VALUES (?, ?, ?, ?)";

      $password = password_hash($confpass, PASSWORD_DEFAULT);
      $stmt = mysqli_stmt_init($conn);
      if (mysqli_stmt_prepare($stmt, $sql))
      {
        mysqli_stmt_bind_param($stmt,"ssss", $email,$password,$name,$lastName);

        mysqli_stmt_execute($stmt);


        $success = true;
      }
    }
    header("Location: realizarCompra.php");

}


$selectSql = "SELECT idUsuario,Name, LastName, email FROM users";
$result = mysqli_query($conn, $selectSql);
?>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Usuarios</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/users.css">
  </head>
  <body>

  <h1>Nuevo Usuario</h1>
    <form method="POST">
      Correo:
      <input type="email" name="EmailTextBox"/>
      </br>
      Contraseña:
      <input type="password" name="PaswordTextBox"/>
      </br>
      Confirma Contraseña:
      <input type="password" name="ConfPaswordTextBox" />
      </br>
      Nombre:
      <input type="text" name="NameTextBox" maxlength="50" />
      </br>
      Apellido:
      <input type="text" name="lastNameTextBox" maxlength="50" />
    </br>
      <input type="submit" name="createNewUserButton" value="Crear" />
    </form>

    <h1>Usuarios</h1>
      <ul>
      <?php
        if (mysqli_num_rows($result) > 0)
        {
         ?>
        <h2>Nombre - Correo</h3>
          <?php
        while( $row = mysqli_fetch_assoc($result) )
        { ?>
          <li><a href="Login.php?id=<?= $row["idUsuario"] ?>"><?= $row["Name"] ?> - <?= $row["email"] ?></a></li>
        <?php
           }
     }
    ?>
    <ul>
    </li>
  </ul>
  </body>
</html>
